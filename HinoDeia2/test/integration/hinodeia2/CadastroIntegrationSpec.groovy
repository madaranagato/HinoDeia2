package hinodeia2

import grails.test.spock.IntegrationSpec

class CadastroIntegrationSpec extends IntegrationSpec {

    def setup() {
    }

    def cleanup() {
    }

    void "cadastrar um cliente"() {
    given:  
        Cliente cliente = new Cliente()
        cliente.nome = nomeCliente
        cliente.email = emailCliente
        cliente.endereco = enderecoCliente
        cliente.telefone = telefoneCliente
        cliente.sexo = sexoCliente
        cliente = cliente.save(flush: true)
        
        ClienteController controller = new ClienteController()
        controller.save()
        
    when:
        controller.save()
        
    than:
        controller.response.status == "CREATED"
    
    where:
        nomeCliente | emailCliente | enderecoCliente | telefoneCliente | sexoCliente
        
        
        
    }
    
    
}
