import hinodeia2.* 

class BootStrap {

    def init = { servletContext ->
        //criacao do usuario em tempo de execucao
        def adminUsuario = Usuario.findByUsername('admin') ?: new Usuario(
            username: 'admin',
            password: 'admin',
            enabled: true
        ).save(failOnError: true)
        
        def regraAdmin = Regra.findByAuthority('REGRA_ADMIN') ?: new Regra(authority:'REGRA_ADMIN').save(failOnError:true)
        
        def regraEmpreendedor = Regra.findByAuthority('REGRA_EMPREENDEDOR') ?: new Regra(authority:'REGRA_EMPREENDEDOR').save(failOnError:true)
        
        if (!adminUsuario.authorities.contains(regraAdmin)){
            UsuarioRegra.create(adminUsuario,regraAdmin)
        }
        
    }
    def destroy = {
    }
}
