package hinodeia2

import org.apache.commons.lang.builder.HashCodeBuilder

class UsuarioRegra implements Serializable {

	private static final long serialVersionUID = 1

	Usuario usuario
	Regra regra

	boolean equals(other) {
		if (!(other instanceof UsuarioRegra)) {
			return false
		}

		other.usuario?.id == usuario?.id &&
		other.regra?.id == regra?.id
	}

	int hashCode() {
		def builder = new HashCodeBuilder()
		if (usuario) builder.append(usuario.id)
		if (regra) builder.append(regra.id)
		builder.toHashCode()
	}

	static UsuarioRegra get(long usuarioId, long regraId) {
		UsuarioRegra.where {
			usuario == Usuario.load(usuarioId) &&
			regra == Regra.load(regraId)
		}.get()
	}

	static boolean exists(long usuarioId, long regraId) {
		UsuarioRegra.where {
			usuario == Usuario.load(usuarioId) &&
			regra == Regra.load(regraId)
		}.count() > 0
	}

	static UsuarioRegra create(Usuario usuario, Regra regra, boolean flush = false) {
		def instance = new UsuarioRegra(usuario: usuario, regra: regra)
		instance.save(flush: flush, insert: true)
		instance
	}

	static boolean remove(Usuario u, Regra r, boolean flush = false) {
		if (u == null || r == null) return false

		int rowCount = UsuarioRegra.where {
			usuario == Usuario.load(u.id) &&
			regra == Regra.load(r.id)
		}.deleteAll()

		if (flush) { UsuarioRegra.withSession { it.flush() } }

		rowCount > 0
	}

	static void removeAll(Usuario u, boolean flush = false) {
		if (u == null) return

		UsuarioRegra.where {
			usuario == Usuario.load(u.id)
		}.deleteAll()

		if (flush) { UsuarioRegra.withSession { it.flush() } }
	}

	static void removeAll(Regra r, boolean flush = false) {
		if (r == null) return

		UsuarioRegra.where {
			regra == Regra.load(r.id)
		}.deleteAll()

		if (flush) { UsuarioRegra.withSession { it.flush() } }
	}

	static constraints = {
		regra validator: { Regra r, UsuarioRegra ur ->
			if (ur.usuario == null) return
			boolean existing = false
			UsuarioRegra.withNewSession {
				existing = UsuarioRegra.exists(ur.usuario.id, r.id)
			}
			if (existing) {
				return 'userRole.exists'
			}
		}
	}

	static mapping = {
		id composite: ['regra', 'usuario']
		version false
	}
}
