package hinodeia2

class Empreendedor extends Usuario{

    String nome
    String email
    
    static constraints = {
        nome nullabel:false, blank: false, maxSize:35, unique: true
        email email:true, unique:true
    }
    
    String toString(){
        nome
    }
}