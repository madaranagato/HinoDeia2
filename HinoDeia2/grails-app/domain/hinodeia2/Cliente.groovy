package hinodeia2

class Cliente {
    
    String nome
    String email
    String endereco
    String telefone
    String sexo
    
    Empreendedor empreendedor
    
    static constraints = {
         nome nullabel:false, blank: false, maxSize:35, unique:true
         email email:true, unique:true
         sexo (inList:["Masculino", "Feminino"])
    }
    
    String toString(){
        nome
    }
}