package hinodeia2

class Anotacao {
    
    String texto
    
    Empreendedor empreendedor
    
    static constraints = {
        texto maxSize: 256
    }
    
    String toString(){
        texto
    }
}
