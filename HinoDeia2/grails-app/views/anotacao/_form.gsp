<%@ page import="hinodeia2.Anotacao" %>



<div class="fieldcontain ${hasErrors(bean: anotacaoInstance, field: 'texto', 'error')} required">
	<label for="texto">
		<g:message code="anotacao.texto.label" default="Texto" />
		<span class="required-indicator">*</span>
	</label>
	<g:textArea name="texto" cols="40" rows="5" maxlength="256" required="" value="${anotacaoInstance?.texto}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: anotacaoInstance, field: 'empreendedor', 'error')} required">
	<label for="empreendedor">
		<g:message code="anotacao.empreendedor.label" default="Empreendedor" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="empreendedor" name="empreendedor.id" from="${hinodeia2.Empreendedor.list()}" optionKey="id" required="" value="${anotacaoInstance?.empreendedor?.id}" class="many-to-one"/>

</div>

