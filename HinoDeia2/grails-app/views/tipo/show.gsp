
<%@ page import="hinodeia2.Tipo" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'tipo.label', default: 'Tipo')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
                 <link rel="stylesheet" href="${resource(dir: 'css', file: 'styles.css')}" type="text/css">
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'flexslider.css')}" type="text/css">
                <script type="text/javascript" src="${resource(dir:'js',file:'scripts.js')}"></script>
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'animate.css')}" type="text/css">
                <script type="text/javascript" src="${resource(dir:'js',file:'jquery.min.js')}"></script>
                <script type="text/javascript" src="${resource(dir:'js',file:'bootstrap.min.js')}"></script>
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap.min.css')}" type="text/css">
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'default.css')}" type="text/css">
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'component.css')}" type="text/css">
                <script type="text/javascript" src="${resource(dir:'js',file:'modernizr.custom.js')}"></script>

	</head>
	<body>
		<a href="#show-tipo" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create_tipo" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-tipo" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list tipo">
			
				<g:if test="${tipoInstance?.descricao}">
				<li class="fieldcontain">
					<span id="descricao-label" class="property-label"><g:message code="tipo.descricao.label" default="Descricao" /></span>
					
						<span class="property-value" aria-labelledby="descricao-label"><g:fieldValue bean="${tipoInstance}" field="descricao"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:tipoInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${tipoInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
