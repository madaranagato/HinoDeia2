<%@ page import="hinodeia2.Tipo" %>



<div class="fieldcontain ${hasErrors(bean: tipoInstance, field: 'descricao', 'error')} required">
	<label for="descricao">
		<g:message code="tipo.descricao.label" default="Descricao" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="descricao" required="" value="${tipoInstance?.descricao}"/>

</div>

