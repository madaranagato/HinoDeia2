
<%@ page import="hinodeia2.Tipo" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'tipo.label', default: 'Tipo')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
                 <link rel="stylesheet" href="${resource(dir: 'css', file: 'styles.css')}" type="text/css">
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'flexslider.css')}" type="text/css">
                <script type="text/javascript" src="${resource(dir:'js',file:'scripts.js')}"></script>
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'animate.css')}" type="text/css">
                <script type="text/javascript" src="${resource(dir:'js',file:'jquery.min.js')}"></script>
                <script type="text/javascript" src="${resource(dir:'js',file:'bootstrap.min.js')}"></script>
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap.min.css')}" type="text/css">
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'default.css')}" type="text/css">
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'component.css')}" type="text/css">
                <script type="text/javascript" src="${resource(dir:'js',file:'modernizr.custom.js')}"></script>

	</head>
	<body>
		<a href="#list-tipo" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="create_tipo" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-tipo" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="descricao" title="${message(code: 'tipo.descricao.label', default: 'Descricao')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${tipoInstanceList}" status="i" var="tipoInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${tipoInstance.id}">${fieldValue(bean: tipoInstance, field: "descricao")}</g:link></td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${tipoInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
