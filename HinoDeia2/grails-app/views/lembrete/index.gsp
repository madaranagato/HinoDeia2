
<%@ page import="hinodeia2.Lembrete" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'lembrete.label', default: 'Lembrete')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-lembrete" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-lembrete" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="data" title="${message(code: 'lembrete.data.label', default: 'Data')}" />
					
						<th><g:message code="lembrete.pedidos.label" default="Pedidos" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${lembreteInstanceList}" status="i" var="lembreteInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${lembreteInstance.id}">${fieldValue(bean: lembreteInstance, field: "data")}</g:link></td>
					
						<td>${fieldValue(bean: lembreteInstance, field: "pedidos")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${lembreteInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
