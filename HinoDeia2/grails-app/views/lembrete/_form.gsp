<%@ page import="hinodeia2.Lembrete" %>



<div class="fieldcontain ${hasErrors(bean: lembreteInstance, field: 'data', 'error')} required">
	<label for="data">
		<g:message code="lembrete.data.label" default="Data" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="data" precision="day"  value="${lembreteInstance?.data}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: lembreteInstance, field: 'pedidos', 'error')} required">
	<label for="pedidos">
		<g:message code="lembrete.pedidos.label" default="Pedidos" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="pedidos" name="pedidos.id" from="${hinodeia2.Pedidos.list()}" optionKey="id" required="" value="${lembreteInstance?.pedidos?.id}" class="many-to-one"/>

</div>

