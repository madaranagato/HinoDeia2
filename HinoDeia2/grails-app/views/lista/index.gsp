<!--
  To change this license header, choose License Headers in Project Properties.
  To change this template file, choose Tools | Templates
  and open the template in the editor.
-->

<%@ page contentType="text/html;charset=UTF-8" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Listando Clientes Cadastrados</title>
        <meta name="layout" content="main"/>
         <link rel="stylesheet" href="${resource(dir: 'css', file: 'styles.css')}" type="text/css">
         <link rel="stylesheet" href="${resource(dir: 'css', file: 'flexslider.css')}" type="text/css">
         <script type="text/javascript" src="${resource(dir:'js',file:'scripts.js')}"></script>
         <link rel="stylesheet" href="${resource(dir: 'css', file: 'animate.css')}" type="text/css">
         <script type="text/javascript" src="${resource(dir:'js',file:'jquery.min.js')}"></script>
         <script type="text/javascript" src="${resource(dir:'js',file:'bootstrap.min.js')}"></script>
         <link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap.min.css')}" type="text/css">
         <link rel="stylesheet" href="${resource(dir: 'css', file: 'default.css')}" type="text/css">
         <link rel="stylesheet" href="${resource(dir: 'css', file: 'component.css')}" type="text/css">
         <script type="text/javascript" src="${resource(dir:'js',file:'modernizr.custom.js')}"></script>

    </head>
    
    <body>
      <center><h1>Lista Empreendedores Cadastrados</h1></center> 
        
      <div class="poslay">   
        <div class="nav">
            <center> <g:render template="busca"/> </center>  
        </div>    

         <div class="nav" id="resultado">
            <center> <g:render template="resultado"/> </center>  
            <g:each in="${consultaNaView}" status="i" var="valor">
             <br/>        
              <table width="300px">  
                <tr>  
                    <td>NOME</td>   
                    <td>EMAIL</td>
                </tr>  
                <tr> 
                    <td>${fieldValue(bean:valor,field:'nome')}<br/></td>
                    <td>${fieldValue(bean:valor, field:'email')} <br/></td>
                </tr>
             </table>
            </g:each> 
        </div>
     </div>   
    </body>
 </html>   