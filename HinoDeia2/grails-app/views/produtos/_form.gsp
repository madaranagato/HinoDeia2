<%@ page import="hinodeia2.Produtos" %>



<div class="fieldcontain ${hasErrors(bean: produtosInstance, field: 'nome', 'error')} required">
	<label for="nome">
		<g:message code="produtos.nome.label" default="Nome" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="nome" required="" value="${produtosInstance?.nome}" placeholder="Informe o nome do produto..."/>

</div>

<div class="fieldcontain ${hasErrors(bean: produtosInstance, field: 'quantidade', 'error')} required">
	<label for="quantidade">
		<g:message code="produtos.quantidade.label" default="Quantidade" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="quantidade" type="number" value="${produtosInstance.quantidade}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: produtosInstance, field: 'tipo', 'error')} required">
	<label for="tipo">
		<g:message code="produtos.tipo.label" default="Tipo" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="tipo" name="tipo.id" from="${hinodeia2.Tipo.list()}" optionKey="id" required="" value="${produtosInstance?.tipo?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: produtosInstance, field: 'valor', 'error')} required">
	<label for="valor">
		<g:message code="produtos.valor.label" default="Valor" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="valor" value="${fieldValue(bean: produtosInstance, field: 'valor')}" required="" />

</div>

