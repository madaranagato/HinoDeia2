
<%@ page import="hinodeia2.Produtos" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'produtos.label', default: 'Produtos')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'styles.css')}" type="text/css">
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'flexslider.css')}" type="text/css">
                <script type="text/javascript" src="${resource(dir:'js',file:'scripts.js')}"></script>
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'animate.css')}" type="text/css">
                <script type="text/javascript" src="${resource(dir:'js',file:'jquery.min.js')}"></script>
                <script type="text/javascript" src="${resource(dir:'js',file:'bootstrap.min.js')}"></script>
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap.min.css')}" type="text/css">
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'default.css')}" type="text/css">
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'component.css')}" type="text/css">
                <script type="text/javascript" src="${resource(dir:'js',file:'modernizr.custom.js')}"></script>

	</head>
	<body>
		<a href="#show-produtos" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create_produto" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-produtos" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list produtos">
			
				<g:if test="${produtosInstance?.nome}">
				<li class="fieldcontain">
					<span id="nome-label" class="property-label"><g:message code="produtos.nome.label" default="Nome" /></span>
					
						<span class="property-value" aria-labelledby="nome-label"><g:fieldValue bean="${produtosInstance}" field="nome"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${produtosInstance?.quantidade}">
				<li class="fieldcontain">
					<span id="quantidade-label" class="property-label"><g:message code="produtos.quantidade.label" default="Quantidade" /></span>
					
						<span class="property-value" aria-labelledby="quantidade-label"><g:fieldValue bean="${produtosInstance}" field="quantidade"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${produtosInstance?.tipo}">
				<li class="fieldcontain">
					<span id="tipo-label" class="property-label"><g:message code="produtos.tipo.label" default="Tipo" /></span>
					
						<span class="property-value" aria-labelledby="tipo-label"><g:link controller="tipo" action="show" id="${produtosInstance?.tipo?.id}">${produtosInstance?.tipo?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${produtosInstance?.valor}">
				<li class="fieldcontain">
					<span id="valor-label" class="property-label"><g:message code="produtos.valor.label" default="Valor" /></span>
					
						<span class="property-value" aria-labelledby="valor-label"><g:fieldValue bean="${produtosInstance}" field="valor"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:produtosInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${produtosInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
