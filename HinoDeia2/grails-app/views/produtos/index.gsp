
<%@ page import="hinodeia2.Produtos" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'produtos.label', default: 'Produtos')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'styles.css')}" type="text/css">
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'flexslider.css')}" type="text/css">
                <script type="text/javascript" src="${resource(dir:'js',file:'scripts.js')}"></script>
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'animate.css')}" type="text/css">
                <script type="text/javascript" src="${resource(dir:'js',file:'jquery.min.js')}"></script>
                <script type="text/javascript" src="${resource(dir:'js',file:'bootstrap.min.js')}"></script>
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap.min.css')}" type="text/css">
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'default.css')}" type="text/css">
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'component.css')}" type="text/css">
                <script type="text/javascript" src="${resource(dir:'js',file:'modernizr.custom.js')}"></script>

	</head>
	<body>
		<a href="#list-produtos" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="create_produto" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-produtos" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="nome" title="${message(code: 'produtos.nome.label', default: 'Nome')}" />
					
						<g:sortableColumn property="quantidade" title="${message(code: 'produtos.quantidade.label', default: 'Quantidade')}" />
					
						<th><g:message code="produtos.tipo.label" default="Tipo" /></th>
					
						<g:sortableColumn property="valor" title="${message(code: 'produtos.valor.label', default: 'Valor')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${produtosInstanceList}" status="i" var="produtosInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${produtosInstance.id}">${fieldValue(bean: produtosInstance, field: "nome")}</g:link></td>
					
						<td>${fieldValue(bean: produtosInstance, field: "quantidade")}</td>
					
						<td>${fieldValue(bean: produtosInstance, field: "tipo")}</td>
					
						<td>${fieldValue(bean: produtosInstance, field: "valor")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${produtosInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
