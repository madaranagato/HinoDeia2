
<%@ page import="hinodeia2.Empreendedor" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'empreendedor.label', default: 'Empreendedor')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'styles.css')}" type="text/css">
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'flexslider.css')}" type="text/css">
                <script type="text/javascript" src="${resource(dir:'js',file:'scripts.js')}"></script>
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'animate.css')}" type="text/css">
                <script type="text/javascript" src="${resource(dir:'js',file:'jquery.min.js')}"></script>
                <script type="text/javascript" src="${resource(dir:'js',file:'bootstrap.min.js')}"></script>
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap.min.css')}" type="text/css">
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'default.css')}" type="text/css">
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'component.css')}" type="text/css">
                <script type="text/javascript" src="${resource(dir:'js',file:'modernizr.custom.js')}"></script>

	</head>
	<body>
		<a href="#list-empreendedor" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>	
				<li><g:link class="create_emp" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
                                <li><g:link class="busca"controller = "Lista" action ="index"> Acessar a lista</g:link></li>
                        </ul>
		</div>
		<div id="list-empreendedor" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="username" title="${message(code: 'empreendedor.username.label', default: 'Username')}" />
					
						<g:sortableColumn property="password" title="${message(code: 'empreendedor.password.label', default: 'Password')}" />
					
						<g:sortableColumn property="nome" title="${message(code: 'empreendedor.nome.label', default: 'Nome')}" />
					
						<g:sortableColumn property="email" title="${message(code: 'empreendedor.email.label', default: 'Email')}" />
					
						<g:sortableColumn property="accountExpired" title="${message(code: 'empreendedor.accountExpired.label', default: 'Account Expired')}" />
					
						<g:sortableColumn property="accountLocked" title="${message(code: 'empreendedor.accountLocked.label', default: 'Account Locked')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${empreendedorInstanceList}" status="i" var="empreendedorInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${empreendedorInstance.id}">${fieldValue(bean: empreendedorInstance, field: "username")}</g:link></td>
					
						<td>${fieldValue(bean: empreendedorInstance, field: "password")}</td>
					
						<td>${fieldValue(bean: empreendedorInstance, field: "nome")}</td>
					
						<td>${fieldValue(bean: empreendedorInstance, field: "email")}</td>
					
						<td><g:formatBoolean boolean="${empreendedorInstance.accountExpired}" /></td>
					
						<td><g:formatBoolean boolean="${empreendedorInstance.accountLocked}" /></td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${empreendedorInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
