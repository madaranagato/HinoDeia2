<%@ page import="hinodeia2.Empreendedor" %>



<div class="fieldcontain ${hasErrors(bean: empreendedorInstance, field: 'username', 'error')} required">
	<label for="username">
		<g:message code="empreendedor.username.label" default="Username" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="username" required="" value="${empreendedorInstance?.username}" placeholder="Digite seu username..."/>

</div>

<div class="fieldcontain ${hasErrors(bean: empreendedorInstance, field: 'password', 'error')} required">
	<label for="password">
		<g:message code="empreendedor.password.label" default="Password" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="password" required="" value="${empreendedorInstance?.password}" placeholder="Digite sua senha..."/>

</div>

<div class="fieldcontain ${hasErrors(bean: empreendedorInstance, field: 'nome', 'error')} required">
	<label for="nome">
		<g:message code="empreendedor.nome.label" default="Nome" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="nome" maxlength="35" required="" value="${empreendedorInstance?.nome}" placeholder="Digite seu nome..."/>

</div>

<div class="fieldcontain ${hasErrors(bean: empreendedorInstance, field: 'email', 'error')} required">
	<label for="email">
		<g:message code="empreendedor.email.label" default="Email" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="email" name="email" required="" value="${empreendedorInstance?.email}" placeholder="Digite seu email..."/>

</div>

<sec:ifAllGranted roles="REGRA_ADMIN">

<div class="fieldcontain ${hasErrors(bean: empreendedorInstance, field: 'accountExpired', 'error')} ">
	<label for="accountExpired">
		<g:message code="empreendedor.accountExpired.label" default="Account Expired" />
		
	</label>
	<g:checkBox name="accountExpired" value="${empreendedorInstance?.accountExpired}" />

</div>

<div class="fieldcontain ${hasErrors(bean: empreendedorInstance, field: 'accountLocked', 'error')} ">
	<label for="accountLocked">
		<g:message code="empreendedor.accountLocked.label" default="Account Locked" />
		
	</label>
	<g:checkBox name="accountLocked" value="${empreendedorInstance?.accountLocked}" />

</div>

<div class="fieldcontain ${hasErrors(bean: empreendedorInstance, field: 'enabled', 'error')} ">
	<label for="enabled">
		<g:message code="empreendedor.enabled.label" default="Enabled" />
		
	</label>
	<g:checkBox name="enabled" value="${empreendedorInstance?.enabled}" />

</div>

<div class="fieldcontain ${hasErrors(bean: empreendedorInstance, field: 'passwordExpired', 'error')} ">
	<label for="passwordExpired">
		<g:message code="empreendedor.passwordExpired.label" default="Password Expired" />
		
	</label>
	<g:checkBox name="passwordExpired" value="${empreendedorInstance?.passwordExpired}" />

</div>
 </sec:ifAllGranted>
