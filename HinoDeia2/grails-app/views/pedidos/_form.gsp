<%@ page import="hinodeia2.Pedidos" %>



<div class="fieldcontain ${hasErrors(bean: pedidosInstance, field: 'formaPagamento', 'error')} required">
	<label for="formaPagamento">
		<g:message code="pedidos.formaPagamento.label" default="Forma Pagamento" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="formaPagamento" from="${pedidosInstance.constraints.formaPagamento.inList}" required="" value="${pedidosInstance?.formaPagamento}" valueMessagePrefix="pedidos.formaPagamento"/>

</div>

<div class="fieldcontain ${hasErrors(bean: pedidosInstance, field: 'dataVencimento', 'error')} required">
	<label for="dataVencimento">
		<g:message code="pedidos.dataVencimento.label" default="Data Vencimento" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="dataVencimento" precision="day"  value="${pedidosInstance?.dataVencimento}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: pedidosInstance, field: 'cliente', 'error')} required">
	<label for="cliente">
		<g:message code="pedidos.cliente.label" default="Cliente" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="cliente" name="cliente.id" from="${hinodeia2.Cliente.list()}" optionKey="id" required="" value="${pedidosInstance?.cliente?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: pedidosInstance, field: 'dataCompra', 'error')} required">
	<label for="dataCompra">
		<g:message code="pedidos.dataCompra.label" default="Data Compra" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="dataCompra" precision="day"  value="${pedidosInstance?.dataCompra}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: pedidosInstance, field: 'produtos', 'error')} ">
	<label for="produtos">
		<g:message code="pedidos.produtos.label" default="Produtos" />
		
	</label>
	<g:select name="produtos" from="${hinodeia2.Produtos.list()}" multiple="multiple" optionKey="id" size="5" value="${pedidosInstance?.produtos*.id}" class="many-to-many"/>

</div>

<div class="fieldcontain ${hasErrors(bean: pedidosInstance, field: 'quantidade', 'error')} required">
	<label for="quantidade">
		<g:message code="pedidos.quantidade.label" default="Quantidade" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="quantidade" type="number" value="${pedidosInstance.quantidade}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: pedidosInstance, field: 'valorTotal', 'error')} required">
	<label for="valorTotal">
		<g:message code="pedidos.valorTotal.label" default="Valor Total" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="valorTotal" value="${fieldValue(bean: pedidosInstance, field: 'valorTotal')}" required=""/>

</div>

