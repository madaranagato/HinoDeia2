
<%@ page import="hinodeia2.Pedidos" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'pedidos.label', default: 'Pedidos')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
                 <link rel="stylesheet" href="${resource(dir: 'css', file: 'styles.css')}" type="text/css">
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'flexslider.css')}" type="text/css">
                <script type="text/javascript" src="${resource(dir:'js',file:'scripts.js')}"></script>
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'animate.css')}" type="text/css">
                <script type="text/javascript" src="${resource(dir:'js',file:'jquery.min.js')}"></script>
                <script type="text/javascript" src="${resource(dir:'js',file:'bootstrap.min.js')}"></script>
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap.min.css')}" type="text/css">
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'default.css')}" type="text/css">
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'component.css')}" type="text/css">
                <script type="text/javascript" src="${resource(dir:'js',file:'modernizr.custom.js')}"></script>

	</head>
	<body>
		<a href="#show-pedidos" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-pedidos" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list pedidos">
			
				<g:if test="${pedidosInstance?.formaPagamento}">
				<li class="fieldcontain">
					<span id="formaPagamento-label" class="property-label"><g:message code="pedidos.formaPagamento.label" default="Forma Pagamento" /></span>
					
						<span class="property-value" aria-labelledby="formaPagamento-label"><g:fieldValue bean="${pedidosInstance}" field="formaPagamento"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${pedidosInstance?.dataVencimento}">
				<li class="fieldcontain">
					<span id="dataVencimento-label" class="property-label"><g:message code="pedidos.dataVencimento.label" default="Data Vencimento" /></span>
					
						<span class="property-value" aria-labelledby="dataVencimento-label"><g:formatDate date="${pedidosInstance?.dataVencimento}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${pedidosInstance?.cliente}">
				<li class="fieldcontain">
					<span id="cliente-label" class="property-label"><g:message code="pedidos.cliente.label" default="Cliente" /></span>
					
						<span class="property-value" aria-labelledby="cliente-label"><g:link controller="cliente" action="show" id="${pedidosInstance?.cliente?.id}">${pedidosInstance?.cliente?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${pedidosInstance?.dataCompra}">
				<li class="fieldcontain">
					<span id="dataCompra-label" class="property-label"><g:message code="pedidos.dataCompra.label" default="Data Compra" /></span>
					
						<span class="property-value" aria-labelledby="dataCompra-label"><g:formatDate date="${pedidosInstance?.dataCompra}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${pedidosInstance?.produtos}">
				<li class="fieldcontain">
					<span id="produtos-label" class="property-label"><g:message code="pedidos.produtos.label" default="Produtos" /></span>
					
						<g:each in="${pedidosInstance.produtos}" var="p">
						<span class="property-value" aria-labelledby="produtos-label"><g:link controller="produtos" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${pedidosInstance?.quantidade}">
				<li class="fieldcontain">
					<span id="quantidade-label" class="property-label"><g:message code="pedidos.quantidade.label" default="Quantidade" /></span>
					
						<span class="property-value" aria-labelledby="quantidade-label"><g:fieldValue bean="${pedidosInstance}" field="quantidade"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${pedidosInstance?.valorTotal}">
				<li class="fieldcontain">
					<span id="valorTotal-label" class="property-label"><g:message code="pedidos.valorTotal.label" default="Valor Total" /></span>
					
						<span class="property-value" aria-labelledby="valorTotal-label"><g:fieldValue bean="${pedidosInstance}" field="valorTotal"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:pedidosInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${pedidosInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
