
<%@ page import="hinodeia2.Pedidos" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'pedidos.label', default: 'Pedidos')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
                
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'styles.css')}" type="text/css">
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'flexslider.css')}" type="text/css">
                <script type="text/javascript" src="${resource(dir:'js',file:'scripts.js')}"></script>
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'animate.css')}" type="text/css">
                <script type="text/javascript" src="${resource(dir:'js',file:'jquery.min.js')}"></script>
                <script type="text/javascript" src="${resource(dir:'js',file:'bootstrap.min.js')}"></script>
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap.min.css')}" type="text/css">
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'default.css')}" type="text/css">
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'component.css')}" type="text/css">
                <script type="text/javascript" src="${resource(dir:'js',file:'modernizr.custom.js')}"></script>

	</head>
	<body>
		<a href="#list-pedidos" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
                        	<li><g:link class="create_pedido" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-pedidos" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="formaPagamento" title="${message(code: 'pedidos.formaPagamento.label', default: 'Forma Pagamento')}" />
					
						<g:sortableColumn property="dataVencimento" title="${message(code: 'pedidos.dataVencimento.label', default: 'Data Vencimento')}" />
					
						<th><g:message code="pedidos.cliente.label" default="Cliente" /></th>
					
						<g:sortableColumn property="dataCompra" title="${message(code: 'pedidos.dataCompra.label', default: 'Data Compra')}" />
					
						<g:sortableColumn property="quantidade" title="${message(code: 'pedidos.quantidade.label', default: 'Quantidade')}" />
					
						<g:sortableColumn property="valorTotal" title="${message(code: 'pedidos.valorTotal.label', default: 'Valor Total')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${pedidosInstanceList}" status="i" var="pedidosInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${pedidosInstance.id}">${fieldValue(bean: pedidosInstance, field: "formaPagamento")}</g:link></td>
					
						<td><g:formatDate date="${pedidosInstance.dataVencimento}" /></td>
					
						<td>${fieldValue(bean: pedidosInstance, field: "cliente")}</td>
					
						<td><g:formatDate date="${pedidosInstance.dataCompra}" /></td>
					
						<td>${fieldValue(bean: pedidosInstance, field: "quantidade")}</td>
					
						<td>${fieldValue(bean: pedidosInstance, field: "valorTotal")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${pedidosInstanceCount ?: 0}" />
			</div>
		</div>
        <script src="js/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->

        <script src="js/bootstrap.min.js"></script>
        <script src="js/scripts.js"></script>
		
	</body>
</html>
