<%@ page import="hinodeia2.UsuarioRegra" %>



<div class="fieldcontain ${hasErrors(bean: usuarioRegraInstance, field: 'regra', 'error')} required">
	<label for="regra">
		<g:message code="usuarioRegra.regra.label" default="Regra" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="regra" name="regra.id" from="${hinodeia2.Regra.list()}" optionKey="id" required="" value="${usuarioRegraInstance?.regra?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: usuarioRegraInstance, field: 'usuario', 'error')} required">
	<label for="usuario">
		<g:message code="usuarioRegra.usuario.label" default="Usuario" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="usuario" name="usuario.id" from="${hinodeia2.Usuario.list()}" optionKey="id" required="" value="${usuarioRegraInstance?.usuario?.id}" class="many-to-one"/>

</div>

