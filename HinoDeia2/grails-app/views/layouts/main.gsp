<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
                <meta name="description" content="Responsive Retina-Friendly Menu with different, size-dependent layouts" />
		<meta name="keywords" content="responsive menu, retina-ready, icon font, media queries, css3, transition, mobile" />
		<title><g:layoutTitle default="Grails"/></title>   
                
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'styles.css')}" type="text/css">
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'flexslider.css')}" type="text/css">
                <script type="text/javascript" src="${resource(dir:'js',file:'scripts.js')}"></script>
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'animate.css')}" type="text/css">
                <script type="text/javascript" src="${resource(dir:'js',file:'jquery.min.js')}"></script>
                <script type="text/javascript" src="${resource(dir:'js',file:'bootstrap.min.js')}"></script>
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap.min.css')}" type="text/css">
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'default.css')}" type="text/css">
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'component.css')}" type="text/css">
                <script type="text/javascript" src="${resource(dir:'js',file:'modernizr.custom.js')}"></script>
  		<asset:stylesheet src="application.css"/>
		<asset:javascript src="application.js"/>
		<g:layoutHead/>
	</head>
        
	<body class="b">
            
            <div class="movimentar">	
			<!-- Codrops top bar -->
                <header class="h">
                    <div class="posic">
                     <div class="col-md-12 text-right navicon">
                           <a id="nav-toggle" class="nav_slide_button" href="#"><span></span></a>
                     </div>
                    </div>
                    
                    <div class="posiTitulo">      
                        <h1 class="animated fadeInDown titulo">HinoDeia</h1>
                             <div class="posSubTitulo">
                                 <p class="animated fadeInUp delay-05s">Pensando em você!</p>
                             </div>    
                    </div>     
                </header>             
            </div>  
  
           <div class="container">     
            <header id="home">
                    <nav>
                        <div class="container-fluid">
                            <nav class="pull">
                                       
                              <div class="main clearfix">

				<nav id="menu" class="navn">					
					<ul class="u">
						<li class="l">
							<a class="a" href="${createLink(uri: '/')}">
								<span class="icon">
									<i aria-hidden="true" class="icon-home"></i>
								</span>
								<span>Menu Principal</span>
							</a>
						</li>
                                              
						<li class="l">
							<a class="a" href="${createLink(uri:'/produtos/index.gsp')}"> 
								<span class="icon"> 
									<i aria-hidden="true" class="icon-services"></i>
								</span>
								<span>Produtos</span>
							</a>
						</li>
                                                
						<li class="l">
							<a class="a" href="${createLink(uri:'/pedidos/index.gsp')}"> 
								<span class="icon">
									<i aria-hidden="true" class="icon-portfolio"></i>
								</span>
								<span>Pedidos</span>
							</a>
						</li>
                                                
						<li class="l">
							<a class="a" href="${createLink(uri:'/cliente/index.gsp')}">  
								<span class="icon">
									<i aria-hidden="true" class="icon-blog"></i>
								</span>
								<span>Cliente</span>
							</a>
						</li>
                                                
                                                <sec:ifAllGranted roles="REGRA_ADMIN">
						<li class="l">
                                                    <a class="a" href="${createLink(uri:'/empreendedor/index.gsp')}"> 
								<span class="icon">
									<i aria-hidden="true" class="icon-team"></i>
								</span>                    
                                                                    <span>Empreendedor</span>                                                         
                                                    </a>
                                                 
						</li>
                                                 </sec:ifAllGranted>
                                                
                                                <li class="l">
							<a  class="a" href="${createLink(uri:'/tipo/index.gsp')}">
								<span class="icon">
									<i aria-hidden="true" class="icon-contact"></i>
								</span>
								<span>Tipo</span>
							</a>
						</li>
                                               
					</ul>
				</nav>     
              
			</div>
                                            
                                    </nav>
                               
   
                        </div>
                    </nav>
                    <section class="hero" id="hero">
                        <div class="poslay">
                            <g:layoutBody/>
                        </div>
                        </div>
                
                    </section>
	</div>
                  
                               <!-- FOOTER -->
                        <footer>
                                <p class="pull-right"><a href="#"><FONT color="Navy">Voltar para o topo.</FONT></a></p>
                                <p>&copy; 2017 Desenvolvedor: Antônio Rufino da Silva Oliveira, Pagina: TecAnime; <a href="#"><FONT color="Navy">Ajuda</FONT></a> &middot; <a href="#"><FONT color="Navy">Privacidade</FONT></a></p>
                        </footer>

		<!-- /container -->
                        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="js/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->

        <script src="js/bootstrap.min.js"></script>
        <script src="js/scripts.js"></script>
		
        </body>        
		
</html>
