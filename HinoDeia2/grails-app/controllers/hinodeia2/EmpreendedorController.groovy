package hinodeia2



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import org.springframework.security.access.annotation.Secured
@Secured('isFullyAuthenticated()')

@Transactional(readOnly = true)
class EmpreendedorController {
    
    MailService mailService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Empreendedor.list(params), model:[empreendedorInstanceCount: Empreendedor.count()]
    }

    def show(Empreendedor empreendedorInstance) {
        respond empreendedorInstance
    }

    def create() {
        respond new Empreendedor(params)
    }

    @Transactional
    def save(Empreendedor empreendedorInstance) {
        if (empreendedorInstance == null) {
            notFound()
            return
        }

        if (empreendedorInstance.hasErrors()) {
            respond empreendedorInstance.errors, view:'create'
            return
        }

        empreendedorInstance.save flush:true

        def regraEmpreendedor=Regra.findByAuthority('REGRA_EMPREENDEDOR')
        
        if (!empreendedorInstance.authorities.contains(regraEmpreendedor)){
            UsuarioRegra.create(empreendedorInstance, regraEmpreendedor)
        }
        //if(!mailService.send("[Projeto]","Participante Cadastrado", ${participanteInstance.email})){
        if(!mailService.send("[Projeto]","Empreendedor Cadastrado", "nagato.uchiha68@gmail.com")){
             println "passou aqui"
            }
        
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'empreendedor.label', default: 'Empreendedor'), empreendedorInstance.id])
                redirect empreendedorInstance
            }
            '*' { respond empreendedorInstance, [status: CREATED] }
        }
    }

    def edit(Empreendedor empreendedorInstance) {
        respond empreendedorInstance
    }

    @Transactional
    def update(Empreendedor empreendedorInstance) {
        if (empreendedorInstance == null) {
            notFound()
            return
        }

        if (empreendedorInstance.hasErrors()) {
            respond empreendedorInstance.errors, view:'edit'
            return
        }

        empreendedorInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Empreendedor.label', default: 'Empreendedor'), empreendedorInstance.id])
                redirect empreendedorInstance
            }
            '*'{ respond empreendedorInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Empreendedor empreendedorInstance) {

        if (empreendedorInstance == null) {
            notFound()
            return
        }

        empreendedorInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Empreendedor.label', default: 'Empreendedor'), empreendedorInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'empreendedor.label', default: 'Empreendedor'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
