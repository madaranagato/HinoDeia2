package hinodeia2

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

import org.springframework.security.access.annotation.Secured
@Secured('isFullyAuthenticated()')


class ListaController {

    def index() { 
      println("passou aqui - index Lista")
        
        def consulta = Empreendedor.findAll()
        
        def nome = Empreendedor.findByNome('Antonio Rufino')
        println "Ant = " + nome
        
        def consultaF = Empreendedor.executeQuery("Select distinct a from Empreendedor a")        
        print consultaF
        
        println("consulta = " +consulta.nome+ "=" +consulta.email)
        render (view:'index', model:[consultaNaView:consulta])
    }
    
    def resultadoAjax = {
        println " Antonio = "+params.nome
        def busca = Empreendedor.findByNomeLike("%${params.nome}%")
    
        if (busca){
            render(template:'resultado', model:[resultado:busca])
        }else{
            flash.message = "Elemento não encontrado!"
            render(template:'resultado', 
            model:[resultado:busca],
            method:'GET')
        }
                
    }
}
