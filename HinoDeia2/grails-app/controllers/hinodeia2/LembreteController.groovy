package hinodeia2



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class LembreteController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Lembrete.list(params), model:[lembreteInstanceCount: Lembrete.count()]
    }

    def show(Lembrete lembreteInstance) {
        respond lembreteInstance
    }

    def create() {
        respond new Lembrete(params)
    }

    @Transactional
    def save(Lembrete lembreteInstance) {
        if (lembreteInstance == null) {
            notFound()
            return
        }

        if (lembreteInstance.hasErrors()) {
            respond lembreteInstance.errors, view:'create'
            return
        }

        lembreteInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'lembrete.label', default: 'Lembrete'), lembreteInstance.id])
                redirect lembreteInstance
            }
            '*' { respond lembreteInstance, [status: CREATED] }
        }
    }

    def edit(Lembrete lembreteInstance) {
        respond lembreteInstance
    }

    @Transactional
    def update(Lembrete lembreteInstance) {
        if (lembreteInstance == null) {
            notFound()
            return
        }

        if (lembreteInstance.hasErrors()) {
            respond lembreteInstance.errors, view:'edit'
            return
        }

        lembreteInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Lembrete.label', default: 'Lembrete'), lembreteInstance.id])
                redirect lembreteInstance
            }
            '*'{ respond lembreteInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Lembrete lembreteInstance) {

        if (lembreteInstance == null) {
            notFound()
            return
        }

        lembreteInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Lembrete.label', default: 'Lembrete'), lembreteInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'lembrete.label', default: 'Lembrete'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
