package hinodeia2



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

import org.springframework.security.access.annotation.Secured
@Secured('isFullyAuthenticated()')

@Transactional(readOnly = true)
class PedidosController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Pedidos.list(params), model:[pedidosInstanceCount: Pedidos.count()]
    }

    def show(Pedidos pedidosInstance) {
        respond pedidosInstance
    }

    def create() {
        respond new Pedidos(params)
    }

    @Transactional
    def save(Pedidos pedidosInstance) {
        if (pedidosInstance == null) {
            notFound()
            return
        }

        if (pedidosInstance.hasErrors()) {
            respond pedidosInstance.errors, view:'create'
            return
        }

        pedidosInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'pedidos.label', default: 'Pedidos'), pedidosInstance.id])
                redirect pedidosInstance
            }
            '*' { respond pedidosInstance, [status: CREATED] }
        }
    }

    def edit(Pedidos pedidosInstance) {
        respond pedidosInstance
    }

    @Transactional
    def update(Pedidos pedidosInstance) {
        if (pedidosInstance == null) {
            notFound()
            return
        }

        if (pedidosInstance.hasErrors()) {
            respond pedidosInstance.errors, view:'edit'
            return
        }

        pedidosInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Pedidos.label', default: 'Pedidos'), pedidosInstance.id])
                redirect pedidosInstance
            }
            '*'{ respond pedidosInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Pedidos pedidosInstance) {

        if (pedidosInstance == null) {
            notFound()
            return
        }

        pedidosInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Pedidos.label', default: 'Pedidos'), pedidosInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'pedidos.label', default: 'Pedidos'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
