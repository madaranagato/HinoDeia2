package hinodeia2



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

import org.springframework.security.access.annotation.Secured
@Secured('isFullyAuthenticated()')

@Transactional(readOnly = true)
class ProdutosController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Produtos.list(params), model:[produtosInstanceCount: Produtos.count()]
    }

    def show(Produtos produtosInstance) {
        respond produtosInstance
    }

    def create() {
        respond new Produtos(params)
    }

    @Transactional
    def save(Produtos produtosInstance) {
        if (produtosInstance == null) {
            notFound()
            return
        }

        if (produtosInstance.hasErrors()) {
            respond produtosInstance.errors, view:'create'
            return
        }

        produtosInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'produtos.label', default: 'Produtos'), produtosInstance.id])
                redirect produtosInstance
            }
            '*' { respond produtosInstance, [status: CREATED] }
        }
    }

    def edit(Produtos produtosInstance) {
        respond produtosInstance
    }

    @Transactional
    def update(Produtos produtosInstance) {
        if (produtosInstance == null) {
            notFound()
            return
        }

        if (produtosInstance.hasErrors()) {
            respond produtosInstance.errors, view:'edit'
            return
        }

        produtosInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Produtos.label', default: 'Produtos'), produtosInstance.id])
                redirect produtosInstance
            }
            '*'{ respond produtosInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Produtos produtosInstance) {

        if (produtosInstance == null) {
            notFound()
            return
        }

        produtosInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Produtos.label', default: 'Produtos'), produtosInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'produtos.label', default: 'Produtos'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
